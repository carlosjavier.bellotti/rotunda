import { Tiger } from "./Tiger.js"

test("Tiger noice", () => {
    const tiger = new Tiger()
    expect(tiger.speak("Hello from the jungle")).toBe("Hello grrr from grrr the grrr jungle grrr")
})