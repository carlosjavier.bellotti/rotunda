import { Animal } from "./Animal.js";

test("Text with noice." ,() => {
    const lion = new Animal("xxx")
    expect(lion.speak("This is a text")).toBe("This xxx is xxx a xxx text xxx")
})

test("Text with irregular spaces and noice." ,() => {
    const lion = new Animal("xxx")
    expect(lion.speak("This      is  a  text")).toBe("This xxx is xxx a xxx text xxx")
})

test("Try to create instance without noice." ,() => {
    expect(() => new Animal("")).toThrow("Noice must be specified.")
})

test("Try with null text." ,() => {
    const lion = new Animal("roar")
    expect(() => lion.speak(null)).toThrow(Error)
    expect(() => lion.speak(null)).toThrow("A text must be specified.")
})

