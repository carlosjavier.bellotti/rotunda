import { Animal } from "./Animal.js";

export class Lion extends Animal {
    constructor() {
        super("roar")
    }
}