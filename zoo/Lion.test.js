import { Lion } from "./Lion.js"

test("Lion noice", () => {
    const lion = new Lion()
    expect(lion.speak("Hello from Africa")).toBe("Hello roar from roar Africa roar")
})