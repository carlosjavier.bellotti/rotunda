export class Animal {
    constructor(noice) {
        if (!noice || !noice.length) throw Error("Noice must be specified.")
        this.noice = ` ${noice} `
    }

    speak(textToSpeak) {
        if (!textToSpeak || !textToSpeak.length) throw new Error("A text must be specified.")
        const words = textToSpeak.trim().split(/\s+/g)
        return words.reduce((result, word) => result += word + this.noice, "").trimEnd()
    }
}