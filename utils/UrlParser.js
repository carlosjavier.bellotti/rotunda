export class UrlParser {
    constructor(urlPattern) {
        this.keys = urlPattern.match(/:\w+/g).map(key => key.substring(1))
        this.regExp = new RegExp(urlPattern.replace(/:\w+/g, "(\\w+)"))
    }

    parse(url) {
        const [ path, paramsString ] = url.split("?")
        // Get query params list
        const queryParams = (paramsString) ? paramsString.split("&") : ""

        // Parse it
        let params = {}
        for (let qp of queryParams) {
            const [param, value] = qp.split("=")
            params[param] = value
        }

        // Extract path variables
        const pathVariablesMatch = path.match(this.regExp)
        if (!pathVariablesMatch) throw new Error("Url doesn't match the pattern.")

        const pathVariables = pathVariablesMatch.slice(1) // Discard first item because it is the entire match
        let variables = {}
        for (let i = 0; i < pathVariables.length; i++) {
            variables[this.keys[i]] = pathVariables[i]
        }
        
        // Compose result
        return { 
            ...variables,
            ...params 
        }
    }
}