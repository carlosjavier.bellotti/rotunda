import { UrlParser } from "./UrlParser.js";

test("Parse a valir url without parameters (/6/api/map/18)", () => {
    const parser = new UrlParser("/:version/api/:collection/:id")
    const result = parser.parse("/6/api/map/18?param1=value1&param2=value2&param3=value3&param4=10.25")
    expect(result.version).toBe("6")
    expect(result.collection).toBe("map")
    expect(result.id).toBe("18")
})

test("Parse a valid url with parameters (/6/api/map/18?param1=value1&param2=value2&param3=value3&param4=10.25)", () => {
    const parser = new UrlParser("/:version/api/:collection/:id")
    const result = parser.parse("/6/api/map/18?param1=value1&param2=value2&param3=value3&param4=10.25")
    expect(result.version).toBe("6")
    expect(result.collection).toBe("map")
    expect(result.id).toBe("18")
    expect(result.param1).toBe("value1")
    expect(result.param2).toBe("value2")
    expect(result.param3).toBe("value3")
    expect(result.param4).toBe("10.25")
})

test("Try to parse and invalid url(/api/list/18?param=1)", () => {
    const parser = new UrlParser("/:version/api/:collection/:id")
    expect(() => parser.parse("/api/list/18?param=1")).toThrow(Error)
    expect(() => parser.parse("/api/list/18?param=1")).toThrow("Url doesn't match the pattern.")
})