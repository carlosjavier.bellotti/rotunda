# Rotunda challenge

## The present project is the solution of the following three excercises 

- Url_parser_exercise
- Zoo_excercise
- Error_alarm_exercise

The first two exercises are implemented in code and you can test these running ```$ npm test``` command.
The Last one is the following pseudo code that represent ```LogError(error)``` function.

```
errorCount = 0
startErrorCount = now()
lastMailSent = 0
function LogError(error) {
    if (startErrorCount > ONE_MINUTE) {
        errorCount = 0
        startErrorCount = now()
    }
    errorCount = errorCount + 1
    if (errorCount >= 10) {
        if (lastMailSent > ONE_MINUTE) {
            sendMail(error)
            lastMailSent = now()
        }
        errorCount = 0
        startErrorCount = now()
    }
    // ... log the error
}
```
